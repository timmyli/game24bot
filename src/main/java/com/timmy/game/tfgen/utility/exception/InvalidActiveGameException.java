package com.timmy.game.tfgen.utility.exception;

public class InvalidActiveGameException extends RuntimeException {
    private static final long serialVersionUID = 138224590316913491L;

    public InvalidActiveGameException(String message) {
        super(message);
    }
}