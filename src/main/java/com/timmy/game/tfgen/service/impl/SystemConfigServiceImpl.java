package com.timmy.game.tfgen.service.impl;

import com.timmy.game.tfgen.dao.entity.QSystemConfigEntity;
import com.timmy.game.tfgen.dao.entity.SystemConfigEntity;
import com.timmy.game.tfgen.dao.repo.SystemConfigRepository;
import com.timmy.game.tfgen.service.SystemConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(value = "transactionManager")
public class SystemConfigServiceImpl implements SystemConfigService {

    @Autowired
    private SystemConfigRepository systemConfigRepository;


    @Override
    public List<SystemConfigEntity> loadAllConfiguration() {
//        return null;
        return systemConfigRepository.findAll();
    }

    @Override
    public List<SystemConfigEntity> getSystemConfigByTag(String tag) {
        return (List<SystemConfigEntity>) systemConfigRepository.findAll(QSystemConfigEntity.systemConfigEntity.tag.eq(tag));
//        return null;
    }
}
