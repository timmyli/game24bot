package com.timmy.game.tfgen.dao.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "game02_game_details")
public class GameDetailsEntity extends BaseEntity{

    @Column(name = "active_ind", columnDefinition = "VARCHAR(255) COMMENT 'tag of the system configuration'")
    private Integer activeInd;

    @Column(name = "round_no", columnDefinition = "VARCHAR(255) COMMENT 'tag of the system configuration'")
    private Integer round;

    @Column(name = "details", columnDefinition = "VARCHAR(255) COMMENT 'tag of the system configuration'")
    private String details;

    @ManyToOne
    @JoinColumn(name = "game_id", referencedColumnName = "id")
    private GameRecordEntity gameRecordEntity;

    public Integer getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(Integer activeInd) {
        this.activeInd = activeInd;
    }

    public Integer getRound() {
        return round;
    }

    public void setRound(Integer round) {
        this.round = round;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public GameRecordEntity getGameRecordEntity() {
        return gameRecordEntity;
    }

    public void setGameRecordEntity(GameRecordEntity gameRecordEntity) {
        this.gameRecordEntity = gameRecordEntity;
    }
}
