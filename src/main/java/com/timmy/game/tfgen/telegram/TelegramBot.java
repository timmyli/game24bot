package com.timmy.game.tfgen.telegram;

import com.timmy.game.tfgen.BotConstant;
import com.timmy.game.tfgen.dao.entity.GameDetailsEntity;
import com.timmy.game.tfgen.dao.entity.GameRecordEntity;
import com.timmy.game.tfgen.dao.entity.SystemConfigEntity;
import com.timmy.game.tfgen.dto.ChatType;
import com.timmy.game.tfgen.dto.GameDTO;
import com.timmy.game.tfgen.service.GameService;
import com.timmy.game.tfgen.service.SystemConfigService;
import com.timmy.game.tfgen.service.TelegramBotService;
import com.timmy.game.tfgen.service.TelegramService;
import com.timmy.game.tfgen.utility.Constants;
import groovy.util.IFileNameFinder;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class TelegramBot extends TelegramLongPollingBot {
    private static final Logger log = LoggerFactory.getLogger(TelegramBot.class);
    private String botUsername;
    private String botToken;

    @Autowired
    SystemConfigService configService;

    @Autowired
    GameService gameService;

    @Autowired
    TelegramService telegramService;

    @Autowired
    TelegramBotService botService;


    @PostConstruct
    private void init() {
        List<SystemConfigEntity> configs = configService.getSystemConfigByTag(BotConstant.TAG_TELEGRAM_BOT);

        for (SystemConfigEntity config : configs){
            if (BotConstant.KEY_BOT_NAME.equals(config.getKey())){
                this.botUsername = config.getValue();
            }
            else if (BotConstant.KEY_BOT_TOKEN.equals(config.getKey())){
                this.botToken = config.getValue();
            }
        }
    }


    public void onUpdateReceived(Update update) {
        SendMessage sendMessage = new SendMessage();
        EditMessageText editMessageText = new EditMessageText();
        try {
            Object message = botService.getExecutableMessage(update);
            if(message instanceof SendMessage){
                sendMessage = (SendMessage) message;
                execute(sendMessage);
            }else if (message instanceof EditMessageText){
                editMessageText = (EditMessageText) message;
                execute(editMessageText);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void callbackQuery(Update update){
        String receivedMessage = update.getCallbackQuery().getData();
        Chat chat = update.getCallbackQuery().getMessage().getChat();
        SendMessage message = new SendMessage();
        String returnMessage = "";
        Long chatId = chat.getId();
        ChatType chatType = chat.isGroupChat()?ChatType.GROUP:ChatType.PRIVATE;

        try {

            if (receivedMessage.startsWith("/")) {
                GameDTO gameDTO = null;
                switch (receivedMessage) {
                    case Constants.TELEGRAM_BOT_COMMAND_NEW_ROUND:
                        gameDTO = gameService.startNewRound(chatId.toString(),chatType);
                        returnMessage =gameDTO.toString();
                        break;
                    case Constants.TELEGRAM_BOT_COMMAND_SOLUTION:
                        gameDTO = gameService.getSolution(chatId.toString(),chatType);
                        returnMessage = gameDTO.getSolution();
                        break;
                    case Constants.TELEGRAM_BOT_COMMAND_ALL_SOLUTION:
                        gameDTO = gameService.getSolution(chatId.toString(),chatType);
                        returnMessage = gameDTO.getAllSolution();
                        break;
                    default:
                        returnMessage ="Wrong commands, please try again.";
                }
            }else {
                if (chatType == ChatType.PRIVATE) {
                    returnMessage = "Please enter a valid command";
                }
            }


            if (!StringUtils.isEmpty(returnMessage)) {
                InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
                List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
                List<InlineKeyboardButton> rowInline = new ArrayList<>();
                rowInline.add(new InlineKeyboardButton().setText("Get Solution").setCallbackData(Constants.TELEGRAM_BOT_COMMAND_SOLUTION));
                // Set the keyboard to the markup
                rowsInline.add(rowInline);
                // Add it to the message
                markupInline.setKeyboard(rowsInline);
                message.setReplyMarkup(markupInline);
                message.setChatId(chatId);
                message.setText(returnMessage);

                execute(message);
            }
        }catch (TelegramApiException e) {
            e.printStackTrace();
        }catch (Exception e){
            returnMessage = e.getMessage();
            message.setChatId(chatId);
            message.setText(returnMessage);

            try {
                execute(message);
            } catch (TelegramApiException ex) {
                ex.printStackTrace();
            }
        }
    }
    public void messageQuery(Update update){
        String receivedMessage = update.getMessage().getText();
        Chat chat = update.getMessage().getChat();
        SendMessage message = new SendMessage();
        String returnMessage = "";
        StringBuilder sb = new StringBuilder();
        String telegramUsername = "";
        boolean isValidUser,isGroupValid = false;
        boolean invalidUserError = false;
        Long chatId = chat.getId();
        ChatType chatType = chat.isGroupChat()?ChatType.GROUP:ChatType.PRIVATE;

        isValidUser = telegramService.isUserExist(chatId.toString());
        isGroupValid = telegramService.isGroupExist(chatId.toString());
        String username = StringUtils.isNotBlank(chat.getUserName())?chat.getUserName(): (chat.getFirstName()+" "+chat.getLastName());
        if(!isValidUser && !isGroupValid){
            if (chatType == ChatType.PRIVATE)
                telegramService.createTelegramUser(chatId.toString(),username);
            else
                telegramService.createGroup(chatId.toString());
        }

        try {

            if (receivedMessage.startsWith("/")) {
                GameDTO gameDTO = null;
                switch (receivedMessage) {
                    case Constants.TELEGRAM_BOT_COMMAND_START:
                        returnMessage = Constants.TELEGRAM_BOT_START_DESC;
                        break;
                    case Constants.TELEGRAM_BOT_COMMAND_HELP:
                        returnMessage =Constants.TELEGRAM_BOT_HELP_DESC;
                        break;
                    case Constants.TELEGRAM_BOT_COMMAND_NEW_GAME:
                        GameRecordEntity gameRecordEntity = gameService.startNewGame(chatId.toString(),chatType);
                        gameDTO = gameService.startNewRound(chatId.toString(),chatType);
                        returnMessage =gameDTO.toString();
                        break;
                    case Constants.TELEGRAM_BOT_COMMAND_NEW_ROUND:
                        gameDTO = gameService.startNewRound(chatId.toString(),chatType);
                        returnMessage =gameDTO.toString();
                        break;
                    case Constants.TELEGRAM_BOT_COMMAND_SOLUTION:
                        gameDTO = gameService.getSolution(chatId.toString(),chatType);
                        returnMessage = gameDTO.getSolution();
                        break;
                    case Constants.TELEGRAM_BOT_COMMAND_ALL_SOLUTION:
                        gameDTO = gameService.getSolution(chatId.toString(),chatType);
                        returnMessage = gameDTO.getAllSolution();
                        break;
                    default:
                        returnMessage ="Wrong commands, please try again.";
                }
            }else {
                if (chatType == ChatType.PRIVATE) {
                    returnMessage = "Please enter a valid command";
                }
            }

            if (!receivedMessage.equals(Constants.TELEGRAM_BOT_COMMAND_SOLUTION) && receivedMessage.startsWith(Constants.TELEGRAM_BOT_COMMAND_SOLUTION)){
                try {
                    GameDTO gameDTO = new GameDTO();
                    GameDetailsEntity temp = new GameDetailsEntity();
                    temp.setDetails(receivedMessage.replace(Constants.TELEGRAM_BOT_COMMAND_SOLUTION,""));
                    gameDTO.setGameDetailsEntity(temp);
                    returnMessage = gameDTO.getSolution();
                }catch (Exception e){
                    returnMessage = "For outside game numbers, please enter command as below format : \n /solution 1,2,3,4";
                }

            }

            if (update.hasCallbackQuery()) {
                log.info("got call back");
            } else {
                log.info("no call back");

            }

            if (!StringUtils.isEmpty(returnMessage)) {
                InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
                List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
                List<InlineKeyboardButton> rowInline = new ArrayList<>();
                rowInline.add(new InlineKeyboardButton().setText("Get Solution").setCallbackData(Constants.TELEGRAM_BOT_COMMAND_SOLUTION));
                // Set the keyboard to the markup
                rowsInline.add(rowInline);
                // Add it to the message
                markupInline.setKeyboard(rowsInline);
                message.setReplyMarkup(markupInline);
                message.setChatId(chatId);
                message.setText(returnMessage);

                execute(message);
            }
        }catch (TelegramApiException e) {
            e.printStackTrace();
        }catch (Exception e){
            returnMessage = e.getMessage();
            message.setChatId(chatId);
            message.setText(returnMessage);

            try {
                execute(message);
            } catch (TelegramApiException ex) {
                ex.printStackTrace();
            }
        }
    }
    @Override
    public String getBotUsername() {
        return botUsername;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }

    public TelegramBot() {
    }

    public synchronized void setButtons(SendMessage sendMessage) {
        // Create a keyboard
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        // Create a list of keyboard rows
        List<KeyboardRow> keyboard = new ArrayList<>();

        // First keyboard row
        KeyboardRow keyboardFirstRow = new KeyboardRow();
        keyboardFirstRow.add(new KeyboardButton("Hi"));

        // Second keyboard row
        KeyboardRow keyboardSecondRow = new KeyboardRow();
        keyboardSecondRow.add(new KeyboardButton("Help"));


        // Add all of the keyboard rows to the list
        keyboard.add(keyboardFirstRow);
        keyboard.add(keyboardSecondRow);
        // and assign this list to our keyboard
        replyKeyboardMarkup.setKeyboard(keyboard);
        sendMessage.setReplyMarkup(replyKeyboardMarkup);

    }

    private void setInline() {
        List<List<InlineKeyboardButton>> buttons = new ArrayList<>();
        List<InlineKeyboardButton> buttons1 = new ArrayList<>();
        buttons1.add(new InlineKeyboardButton().setText("Button").setCallbackData("Test"));
        buttons.add(buttons1);

        InlineKeyboardMarkup markupKeyboard = new InlineKeyboardMarkup();
        markupKeyboard.setKeyboard(buttons);
    }



}
