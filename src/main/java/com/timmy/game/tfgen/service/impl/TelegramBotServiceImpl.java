package com.timmy.game.tfgen.service.impl;

import com.timmy.game.tfgen.dao.entity.GameDetailsEntity;
import com.timmy.game.tfgen.dao.entity.GameRecordEntity;
import com.timmy.game.tfgen.dto.ChatType;
import com.timmy.game.tfgen.dto.GameDTO;
import com.timmy.game.tfgen.service.GameService;
import com.timmy.game.tfgen.service.TelegramBotService;
import com.timmy.game.tfgen.utility.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@Transactional(value = "transactionManager")
public class TelegramBotServiceImpl implements TelegramBotService {

    @Autowired
    GameService gameService;
    @Override
    public String processReturnMessage(Update update) {
        Chat chat = getChatId(update);
        ChatType chatType = chat.isGroupChat() ? ChatType.GROUP : ChatType.PRIVATE;

        String receivedMessage = getReceivedMessage(update);
        String chatId = chat.getId().toString();
        String returnMessage = "";
        boolean callbackFlag = false;


        try {

            if (receivedMessage.startsWith("/")) {
                GameDTO gameDTO = new GameDTO();
                switch (receivedMessage) {
                    case Constants.TELEGRAM_BOT_COMMAND_START:
                        returnMessage = Constants.TELEGRAM_BOT_START_DESC;
                        break;
                    case Constants.TELEGRAM_BOT_COMMAND_HELP:
                        returnMessage =Constants.TELEGRAM_BOT_HELP_DESC;
                        break;
                    case Constants.TELEGRAM_BOT_COMMAND_NEW_GAME:
                        GameRecordEntity gameRecordEntity = gameService.startNewGame(chatId.toString(),chatType);
                        gameDTO = gameService.startNewRound(chatId,chatType);
                        returnMessage =gameDTO.toString();
                        break;
                    case Constants.TELEGRAM_BOT_COMMAND_NEW_ROUND:
                        gameDTO = gameService.startNewRound(chatId,chatType);
                        returnMessage =gameDTO.toString();
                        break;
                    case Constants.TELEGRAM_BOT_COMMAND_SOLUTION:
                        gameDTO = gameService.getSolution(chatId,chatType);
                        returnMessage = gameDTO.getSolution();
                        break;
                    case Constants.TELEGRAM_BOT_COMMAND_ALL_SOLUTION:
                        gameDTO = gameService.getSolution(chatId,chatType);
                        returnMessage = gameDTO.getAllSolution();
                        break;
                        //Call back functions starts from here
                    case Constants.TELEGRAM_BOT_COMMAND_CALLBACK_SOLUTION:
                        gameDTO.setGameDetailsEntity(formDetailsEntityFromMessageForCallback(update.getCallbackQuery().getMessage().getText()));
                        returnMessage = gameDTO.getSolution();
                        callbackFlag = true;
                        break;
                    case Constants.TELEGRAM_BOT_COMMAND_CALLBACK_ALL_SOLUTION:
                        gameDTO.setGameDetailsEntity(formDetailsEntityFromMessageForCallback(update.getCallbackQuery().getMessage().getText()));
                        returnMessage = gameDTO.getAllSolution();
                        callbackFlag = true;
                        break;
                    default:
                        returnMessage ="Wrong commands, please try again.";
                }
            }else {
                if (chatType == ChatType.PRIVATE) {
                    returnMessage = "Please enter a valid command";
                }
            }
            if (callbackFlag){
                String originalMessage = update.getCallbackQuery().getMessage().getText();
                switch (receivedMessage) {
                    case Constants.TELEGRAM_BOT_COMMAND_CALLBACK_SOLUTION:
                        returnMessage = originalMessage +"\n\nSolution : \n" +returnMessage;
                        break;
                    case Constants.TELEGRAM_BOT_COMMAND_CALLBACK_ALL_SOLUTION:
                        returnMessage = originalMessage +"\n\nAll solutions : \n" +returnMessage;
                }
            }
        }catch (Exception e){
            e.printStackTrace();;
            returnMessage = e.getMessage();
        }
        return returnMessage;
    }

    @Override
    public Object getExecutableMessage(Update update) {
        String chatId = getChatId(update).getId().toString();
        String returnMessage =processReturnMessage(update);
        String receivedMessage = getReceivedMessage(update);
        InlineKeyboardMarkup inlineKeyboardMarkup = getInlineMakeUp(receivedMessage);
        Object message = new Object();
        SendMessage sendMessage = new SendMessage();
        if (!update.hasCallbackQuery()){
            sendMessage.setText(returnMessage);
            sendMessage.setChatId(chatId);
            if (inlineKeyboardMarkup!=null){
                sendMessage.setReplyMarkup(inlineKeyboardMarkup);
            }
            message = sendMessage;
        }else {
            if (receivedMessage.equals(Constants.TELEGRAM_BOT_COMMAND_CALLBACK_SOLUTION) || receivedMessage.equals(Constants.TELEGRAM_BOT_COMMAND_CALLBACK_ALL_SOLUTION)){
                long messageId =  update.getCallbackQuery().getMessage().getMessageId();
                EditMessageText new_message = new EditMessageText()
                        .setChatId(chatId)
                        .setMessageId(Math.toIntExact(messageId))
                        .setText(returnMessage);
                new_message.setReplyMarkup(inlineKeyboardMarkup);
                message = new_message;
            }else {
                sendMessage.setText(returnMessage);
                sendMessage.setChatId(chatId);
                if (inlineKeyboardMarkup!=null){
                    sendMessage.setReplyMarkup(inlineKeyboardMarkup);
                }
                message = sendMessage;
            }

        }
        return message;
    }

    private String getReceivedMessage(Update update){
        String message = "";
        if (update.getMessage()!=null){
            message =  update.getMessage().getText();
        }else {
            message = update.getCallbackQuery().getData();
        }
        return message;
    }

    private Chat getChatId(Update update){
        Chat chat = null;
        if (update.getMessage()!=null){
            chat =  update.getMessage().getChat();
        }else {
            chat = update.getCallbackQuery().getMessage().getChat();
        }
        return chat;
    }

    private GameDetailsEntity formDetailsEntityFromMessageForCallback(String message){
        message = parse(message);
        GameDetailsEntity gameDetailsEntity = new GameDetailsEntity();
        gameDetailsEntity.setDetails(message);

        return gameDetailsEntity;
    }

    private InlineKeyboardMarkup getInlineMakeUp (String receivedMessage) {
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> rowInline = new ArrayList<>();



        if (receivedMessage.startsWith("/")) {
            switch (receivedMessage) {
                case Constants.TELEGRAM_BOT_COMMAND_START:
                    rowInline.add(new InlineKeyboardButton().setText("See All Commands").setCallbackData(Constants.TELEGRAM_BOT_COMMAND_HELP));
                    rowsInline.add(rowInline);
                    break;
                case Constants.TELEGRAM_BOT_COMMAND_NEW_GAME:
                    rowInline.add(new InlineKeyboardButton().setText("Solution").setCallbackData(Constants.TELEGRAM_BOT_COMMAND_CALLBACK_SOLUTION));
                    rowInline.add(new InlineKeyboardButton().setText("New round").setCallbackData(Constants.TELEGRAM_BOT_COMMAND_NEW_ROUND));
                    rowsInline.add(rowInline);
                    break;
                case Constants.TELEGRAM_BOT_COMMAND_NEW_ROUND:
                    rowInline.add(new InlineKeyboardButton().setText("Solution").setCallbackData(Constants.TELEGRAM_BOT_COMMAND_CALLBACK_SOLUTION));
                    rowInline.add(new InlineKeyboardButton().setText("New round").setCallbackData(Constants.TELEGRAM_BOT_COMMAND_NEW_ROUND));
                    rowsInline.add(rowInline);
                    break;
                case Constants.TELEGRAM_BOT_COMMAND_SOLUTION:
                    rowInline.add(new InlineKeyboardButton().setText("See All Solutions").setCallbackData(Constants.TELEGRAM_BOT_COMMAND_CALLBACK_ALL_SOLUTION));
                    rowInline.add(new InlineKeyboardButton().setText("New round").setCallbackData(Constants.TELEGRAM_BOT_COMMAND_NEW_ROUND));
                    rowsInline.add(rowInline);
                    break;
                case Constants.TELEGRAM_BOT_COMMAND_ALL_SOLUTION:
                    rowInline.add(new InlineKeyboardButton().setText("New round").setCallbackData(Constants.TELEGRAM_BOT_COMMAND_NEW_ROUND));
                    rowsInline.add(rowInline);
                    break;
                //Call back functions starts from here
                case Constants.TELEGRAM_BOT_COMMAND_CALLBACK_SOLUTION:
                    rowInline.add(new InlineKeyboardButton().setText("See All Solutions").setCallbackData(Constants.TELEGRAM_BOT_COMMAND_CALLBACK_ALL_SOLUTION));
                    rowInline.add(new InlineKeyboardButton().setText("New round").setCallbackData(Constants.TELEGRAM_BOT_COMMAND_NEW_ROUND));
                    rowsInline.add(rowInline);
                    break;
                case Constants.TELEGRAM_BOT_COMMAND_CALLBACK_ALL_SOLUTION:
                    rowInline.add(new InlineKeyboardButton().setText("New round").setCallbackData(Constants.TELEGRAM_BOT_COMMAND_NEW_ROUND));
                    rowsInline.add(rowInline);
                    break;
                default:
                    return null;
            }
        }
        else {
            return null;
        }
        markupInline.setKeyboard(rowsInline);
        return markupInline;
    }

    public static String parse(String in) {
        String returnString = "";
        in = in.substring(in.indexOf("[")+1,in.indexOf("]"));
        return in;
    }
}
