package com.timmy.game.tfgen.dao.repo;

import com.timmy.game.tfgen.dao.entity.GameRecordEntity;
import com.timmy.game.tfgen.dao.entity.SystemConfigEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

/**
 * Created by Li Yi on 4/9/2017.
 */
public interface GameRecordRepository extends JpaRepository<GameRecordEntity, Integer>, QuerydslPredicateExecutor<GameRecordEntity> {
}