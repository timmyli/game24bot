package com.timmy.game.tfgen.dao.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "telegram02_group")
public class TelegramGroupEntity extends BaseEntity{

    @Column(name = "group_chat_id", columnDefinition = "VARCHAR(255) COMMENT 'tag of the system configuration'")
    private String groupChatId;

    @OneToMany(mappedBy = "telegramGroupEntity", fetch = FetchType.LAZY)
    private List<GameRecordEntity> gameRecordEntities = new ArrayList<>();

    public List<GameRecordEntity> getGameRecordEntities() {
        return gameRecordEntities;
    }

    public void setGameRecordEntities(List<GameRecordEntity> gameRecordEntities) {
        this.gameRecordEntities = gameRecordEntities;
    }

    public String getGroupChatId() {
        return groupChatId;
    }

    public void setGroupChatId(String groupChatId) {
        this.groupChatId = groupChatId;
    }
}
