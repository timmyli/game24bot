package com.timmy.game.tfgen.dto;

public enum ChatType {
    PRIVATE("private"),
    GROUP("group");

    private String chatType;

    ChatType(String chatType){
        this.chatType = chatType;
    }

    public String getChatType() {
        return chatType;
    }
}
