package com.timmy.game.tfgen.utility.exception;

public class InvalidTelegramBotUserException extends RuntimeException {
    private static final long serialVersionUID = 138224590316913491L;

    public InvalidTelegramBotUserException(String message) {
        super(message);
    }
}