package com.timmy.game.tfgen.utility;

public class Constants {
    public static final String TELEGRAM_BOT_COMMAND_HELP = "/help";
    public static final String TELEGRAM_BOT_COMMAND_NEW_GAME = "/newgame";
    public static final String TELEGRAM_BOT_COMMAND_NEW_ROUND = "/newround";
    public static final String TELEGRAM_BOT_COMMAND_SOLUTION = "/solution";
    public static final String TELEGRAM_BOT_COMMAND_ALL_SOLUTION = "/allsolution";
    public static final String TELEGRAM_BOT_COMMAND_CALLBACK_SOLUTION = "/callbackSolution";
    public static final String TELEGRAM_BOT_COMMAND_CALLBACK_ALL_SOLUTION = "/callbackAllsolution";
    public static final String TELEGRAM_BOT_COMMAND_START = "/start";

    public static final String TELEGRAM_BOT_START_DESC = "Welcome to 24 game generator, system will give you few random numbers, please use any combination to form 24, every number has to be used once and only once. \n\nIf you need any help about the commands,  please use "+"/help for more information about the commands.";
    public static final String TELEGRAM_BOT_HELP_DESC = "Command List : \n\n"+"/help : See list of commands \n\n"+"/newgame : Start a new game session\n\n"
//            +"/newgame ? ? ? : Start a new game with limited range of number, for example '/newgame 1 10 4', for this game session, system will generate 4 random numbers from 1 to 10. \n\n"
            +"/newround : Start a new round of game in current game session\n\n"
            +"/solution : Display one solution for last round of game. \n\n" +"/solution ? ? ? ? : This will solve the 24 solution based on the numbers you entered, example : '/solution 1 2 3 4' \n\n"+
            "/allsolution : Display all possible solution for last round of game.";
}
