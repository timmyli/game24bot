package com.timmy.game.tfgen.dao.repo;

import com.timmy.game.tfgen.dao.entity.SystemConfigEntity;
import com.timmy.game.tfgen.dao.entity.TelegramUserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

/**
 * Created by Li Yi on 4/9/2017.
 */
public interface TelegramUserRepository extends JpaRepository<TelegramUserEntity, Integer>, QuerydslPredicateExecutor<TelegramUserEntity> {
}