package com.timmy.game.tfgen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.telegram.telegrambots.ApiContextInitializer;

@SpringBootApplication
public class ApibotApplication {

    public static void main(String[] args) {
        ApiContextInitializer.init();
        SpringApplication.run(ApibotApplication.class, args);
    }

}
