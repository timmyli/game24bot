package com.timmy.game.tfgen.service;

import com.timmy.game.tfgen.dao.entity.SystemConfigEntity;

import java.util.List;

public interface SystemConfigService {
   List<SystemConfigEntity> loadAllConfiguration();

   List<SystemConfigEntity> getSystemConfigByTag(String tag);
}
