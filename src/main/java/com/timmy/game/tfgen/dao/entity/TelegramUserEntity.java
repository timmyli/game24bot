package com.timmy.game.tfgen.dao.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "telegram01_user")
public class TelegramUserEntity extends BaseEntity{

    @Column(name = "telegram_id", columnDefinition = "VARCHAR(255) COMMENT 'tag of the system configuration'")
    private String telegramId;

    @Column(name = "telegram_chat_id", columnDefinition = "VARCHAR(255) COMMENT 'tag of the system configuration'")
    private String telegramChatId;

    @Column(name = "username", columnDefinition = "VARCHAR(255) COMMENT 'tag of the system configuration'")
    private String username;

    @OneToMany(mappedBy = "telegramUserEntity", fetch = FetchType.LAZY)
    private List<GameRecordEntity> gameRecordEntities = new ArrayList<>();

    public String getTelegramId() {
        return telegramId;
    }

    public void setTelegramId(String telegramId) {
        this.telegramId = telegramId;
    }

    public List<GameRecordEntity> getGameRecordEntities() {
        return gameRecordEntities;
    }

    public void setGameRecordEntities(List<GameRecordEntity> gameRecordEntities) {
        this.gameRecordEntities = gameRecordEntities;
    }

    public String getTelegramChatId() {
        return telegramChatId;
    }

    public void setTelegramChatId(String telegramChatId) {
        this.telegramChatId = telegramChatId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
