package com.timmy.game.tfgen.dto;

import com.timmy.game.tfgen.dao.entity.GameDetailsEntity;
import com.timmy.game.tfgen.dao.entity.GameRecordEntity;
import com.timmy.game.tfgen.utility.GameSolverHelper;

import java.util.ArrayList;
import java.util.List;

public class GameDTO {
    private GameDetailsEntity gameDetailsEntity;
    private GameRecordEntity gameRecordEntity;

    public GameDetailsEntity getGameDetailsEntity() {
        return gameDetailsEntity;
    }

    public void setGameDetailsEntity(GameDetailsEntity gameDetailsEntity) {
        this.gameDetailsEntity = gameDetailsEntity;
    }

    public GameRecordEntity getGameRecordEntity() {
        return gameRecordEntity;
    }

    public void setGameRecordEntity(GameRecordEntity gameRecordEntity) {
        this.gameRecordEntity = gameRecordEntity;
    }

    @Override
    public String toString() {
        if (gameDetailsEntity.getRound() == 1)
        return "New Game started, four numbers as follow : \n [ "+
                gameDetailsEntity.getDetails()+" ]";
        else  return "Round "+gameDetailsEntity.getRound()+" started, four numbers as follow : \n [ "+
                gameDetailsEntity.getDetails()+" ]";
    }

    public String getSolution(){
        String[] listInt = gameDetailsEntity.getDetails().trim().split(",");
        List<Integer> gameInt = new ArrayList<>();
        for (String s : listInt){
            gameInt.add(Integer.parseInt(s.trim()));
        }
        String solution = GameSolverHelper.getSolution(gameInt);
        if (gameDetailsEntity!=null){
            return "Solution for [ "+gameDetailsEntity.getDetails()+" ] is : \n"+ solution;
        }else {
            return "Please start a game first";
        }
    }

    public String getAllSolution(){
        String[] listInt = gameDetailsEntity.getDetails().trim().split(",");
        List<Integer> gameInt = new ArrayList<>();
        for (String s : listInt){
            gameInt.add(Integer.parseInt(s.trim()));
        }
        String solution = GameSolverHelper.getAllSolution(gameInt);
        if (gameDetailsEntity!=null){
            return "Solution for [ "+gameDetailsEntity.getDetails()+" ] is : \n"+ solution;
        }else {
            return "Please start a game first";
        }
    }
}
