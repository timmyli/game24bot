package com.timmy.game.tfgen.service.impl;

import com.querydsl.core.BooleanBuilder;
import com.timmy.game.tfgen.dao.entity.*;
import com.timmy.game.tfgen.dao.repo.GameDetailsRepository;
import com.timmy.game.tfgen.dao.repo.GameRecordRepository;
import com.timmy.game.tfgen.dao.repo.TelegramGroupRepository;
import com.timmy.game.tfgen.dao.repo.TelegramUserRepository;
import com.timmy.game.tfgen.dto.ChatType;
import com.timmy.game.tfgen.dto.GameDTO;
import com.timmy.game.tfgen.service.GameService;
import com.timmy.game.tfgen.service.TelegramService;
import com.timmy.game.tfgen.utility.Helper;
import com.timmy.game.tfgen.utility.exception.InvalidActiveGameException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
@Transactional(value = "transactionManager")
public class GameServiceImpl implements GameService {

    @Autowired
    private TelegramGroupRepository groupRepository;

    @Autowired
    private TelegramUserRepository userRepository;

    @Autowired
    private GameRecordRepository gameRecordRepository;

    @Autowired
    private GameDetailsRepository gameDetailsRepository;

    @Autowired
    private TelegramService telegramService;

    @Override
    public GameRecordEntity startNewGame(String chatId, ChatType chatType) {
        TelegramGroupEntity telegramGroupEntity;
        TelegramUserEntity telegramUserEntity;
        GameRecordEntity gameRecordEntity  = new GameRecordEntity();
        if (chatType == ChatType.GROUP){
            telegramGroupEntity = getGroupEntity(chatId);
            if(telegramGroupEntity == null){
                telegramGroupEntity = createNewGroupEntity(chatId);
            }
            inactivePreviousGame(telegramGroupEntity);
            gameRecordEntity.setActiveInd(0);
            gameRecordEntity.setTelegramGroupEntity(telegramGroupEntity);
            gameRecordEntity.setCreatedBy(0L);
            gameRecordEntity.setCreatedDt(new Date());
        }else if (chatType == ChatType.PRIVATE){
            telegramUserEntity = getUserEntity(chatId);
            inactivePreviousGame(telegramUserEntity);
            gameRecordEntity.setActiveInd(0);
            gameRecordEntity.setTelegramUserEntity(telegramUserEntity);
            gameRecordEntity.setCreatedBy(0L);
            gameRecordEntity.setCreatedDt(new Date());
        }
        gameRecordEntity =  gameRecordRepository.save(gameRecordEntity);
        return gameRecordEntity;
    }

    @Override
    public GameDTO startNewRound(String chatId, ChatType chatType) {
        GameDetailsEntity gameDetailsEntity = new GameDetailsEntity();
        GameRecordEntity gameRecordEntity = getActiveGame(chatId);
        GameDTO gameDTO = new GameDTO();
        if (gameRecordEntity !=null) {
            GameDetailsEntity previousGame  = inactivePreviousRoundGame(gameRecordEntity);
            gameDetailsEntity.setGameRecordEntity(gameRecordEntity);
            if (previousGame!=null) {
                gameDetailsEntity.setRound(previousGame.getRound()+1);
            }else {
                gameDetailsEntity.setRound(1);
            }
            gameDetailsEntity.setActiveInd(0);
            gameDetailsEntity.setDetails(Helper.randomListToString());
            gameDetailsEntity.setCreatedBy(0L);
            gameDetailsEntity.setCreatedDt(new Date());
            gameDetailsEntity = gameDetailsRepository.save(gameDetailsEntity);
        }else {
            throw new InvalidActiveGameException("There is no active game, please start a game first.");
        }
        gameDTO.setGameDetailsEntity(gameDetailsEntity);
        gameDTO.setGameRecordEntity(gameRecordEntity);
        return gameDTO;
    }

    @Override
    public GameDTO getSolution(String chatId, ChatType chatType) {
        GameRecordEntity gameRecordEntity = getActiveGame(chatId);
        GameDetailsEntity gameDetailsEntity = getActiveRound(chatId);
        GameDTO gameDTO = new GameDTO();
        gameDTO.setGameRecordEntity(gameRecordEntity);
        gameDTO.setGameDetailsEntity(gameDetailsEntity);
        return gameDTO;
    }

    @Override
    public GameDTO getAllSolution(String chatId, ChatType chatType) {
        return null;
    }

    private TelegramGroupEntity getGroupEntity(String chatId){
        BooleanBuilder booleanBuilder = new BooleanBuilder();
        QTelegramGroupEntity qTelegramGroupEntity = QTelegramGroupEntity.telegramGroupEntity;
        booleanBuilder.and(qTelegramGroupEntity.groupChatId.eq(chatId));
        TelegramGroupEntity telegramGroupEntity = groupRepository.findOne(booleanBuilder).orElse(null);

        return telegramGroupEntity;
    }

    private TelegramUserEntity getUserEntity(String chatId){
        BooleanBuilder booleanBuilder = new BooleanBuilder();
        QTelegramUserEntity qTelegramUserEntity = QTelegramUserEntity.telegramUserEntity;
        booleanBuilder.and(qTelegramUserEntity.telegramId.eq(chatId));
        TelegramUserEntity telegramUserEntity = userRepository.findOne(booleanBuilder).orElse(null);

        return telegramUserEntity;
    }

    private void inactivePreviousGame(Object telegramObject){
        BooleanBuilder booleanBuilder = new BooleanBuilder();
        QGameRecordEntity qGameRecordEntity = QGameRecordEntity.gameRecordEntity;
        booleanBuilder.and(qGameRecordEntity.activeInd.eq(0));
        GameRecordEntity gameRecordEntity = null;
        if (telegramObject instanceof TelegramUserEntity){
            booleanBuilder.and(qGameRecordEntity.telegramUserEntity.eq((TelegramUserEntity) telegramObject));
        }else if (telegramObject instanceof  TelegramGroupEntity){
            booleanBuilder.and(qGameRecordEntity.telegramGroupEntity.eq((TelegramGroupEntity) telegramObject));
        }
        gameRecordEntity = gameRecordRepository.findOne(booleanBuilder).orElse(null);
        if (gameRecordEntity!=null){
            gameRecordEntity.setActiveInd(1);
            gameRecordRepository.save(gameRecordEntity);
        }
    }

    private GameRecordEntity getActiveGame(String chatId){
        TelegramUserEntity telegramUserEntity = getUserEntity(chatId);
        TelegramGroupEntity telegramGroupEntity = getGroupEntity(chatId);
        BooleanBuilder booleanBuilder = new BooleanBuilder();
        QGameRecordEntity qGameRecordEntity = QGameRecordEntity.gameRecordEntity;
        booleanBuilder.and(qGameRecordEntity.activeInd.eq(0));
        if (telegramUserEntity !=null){
            booleanBuilder.and(qGameRecordEntity.telegramUserEntity.eq(telegramUserEntity));
        }else if (telegramGroupEntity != null){
            booleanBuilder.and(qGameRecordEntity.telegramGroupEntity.eq(telegramGroupEntity));
        }
        GameRecordEntity gameRecordEntity = gameRecordRepository.findOne(booleanBuilder).orElse(null);
        return gameRecordEntity;
    }

    private GameDetailsEntity getActiveRound(String chatId){
        BooleanBuilder booleanBuilder = new BooleanBuilder();
        QGameDetailsEntity qGameDetailsEntity = QGameDetailsEntity.gameDetailsEntity;
        booleanBuilder.and(qGameDetailsEntity.activeInd.eq(0));
        GameRecordEntity gameRecordEntity = getActiveGame(chatId);
        booleanBuilder.and(qGameDetailsEntity.gameRecordEntity.eq(gameRecordEntity));
        return gameDetailsRepository.findOne(booleanBuilder).orElse(null);
    }

    private GameDetailsEntity inactivePreviousRoundGame(GameRecordEntity gameRecordEntity){
        BooleanBuilder booleanBuilder = new BooleanBuilder();
        QGameDetailsEntity qGameDetailsEntity = QGameDetailsEntity.gameDetailsEntity;
        booleanBuilder.and(qGameDetailsEntity.activeInd.eq(0));
        booleanBuilder.and(qGameDetailsEntity.gameRecordEntity.eq(gameRecordEntity));
        GameDetailsEntity gameDetailsEntity = gameDetailsRepository.findOne(booleanBuilder).orElse(null);
        if (gameDetailsEntity!=null){
            gameDetailsEntity.setActiveInd(1);
            gameDetailsEntity = gameDetailsRepository.save(gameDetailsEntity);
        }
        return gameDetailsEntity;
    }

    private TelegramGroupEntity createNewGroupEntity (String chatId){
        TelegramGroupEntity telegramGroupEntity = new TelegramGroupEntity();
        telegramGroupEntity.setCreatedBy(0L);
        telegramGroupEntity.setCreatedDt(new Date());
        telegramGroupEntity.setGroupChatId(chatId);
        return groupRepository.save(telegramGroupEntity);
    }

}
