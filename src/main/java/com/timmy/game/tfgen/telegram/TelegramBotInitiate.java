package com.timmy.game.tfgen.telegram;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.generics.BotSession;

import javax.annotation.PostConstruct;

@Configuration
public class TelegramBotInitiate {
    private static final Logger LOG = LoggerFactory.getLogger(TelegramBotInitiate.class);

    @Autowired
    TelegramBot telegramBot;

    @PostConstruct
    private void init() {

        initialTelegramBot();
    }

    private void initialTelegramBot(){

        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
        try {
            BotSession botSession = telegramBotsApi.registerBot(telegramBot);

        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
