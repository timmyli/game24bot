package com.timmy.game.tfgen.utility.exception;

import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        List<StringBuilder> res = new ArrayList<StringBuilder>();
        int[] arr = {1,24,2,1};
        int target = 24;
        for(int i = 0; i < arr.length; i ++){
            boolean[] visited = new boolean[arr.length];
            visited[i] = true;
            StringBuilder sb = new StringBuilder();
            sb.append(arr[i]);
            findMatch(res, sb, arr, visited, arr[i], "+-*/", target);
        }

        for(StringBuilder sb : res){
            System.out.println(sb.toString());
        }
    }

    public static void findMatch(List<StringBuilder> res, StringBuilder temp, int[] nums, boolean[] visited, int current, String operations, int target) {
        if (current == target) {
            res.add(new StringBuilder(temp));
        }

        for (int i = 0; i < nums.length; i++) {
            if (visited[i]) continue;
            for (char c : operations.toCharArray()) {
                visited[i] = true;
                temp.append(c).append(nums[i]);
                if (c == '+') {
                    findMatch(res, temp, nums, visited, current + nums[i], operations, target);
                } else if (c == '-') {
                    findMatch(res, temp, nums, visited, current - nums[i], operations, target);
                } else if (c == '*') {
                    findMatch(res, temp, nums, visited, current * nums[i], operations, target);
                } else if (c == '/') {
                    findMatch(res, temp, nums, visited, current / nums[i], operations, target);
                }
                temp.delete(temp.length() - 2, temp.length());
                visited[i] = false;
            }
        }
    }
    }
