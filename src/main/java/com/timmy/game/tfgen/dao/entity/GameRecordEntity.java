package com.timmy.game.tfgen.dao.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "game01_game_record")
public class GameRecordEntity extends BaseEntity{

    @Column(name = "active_ind", columnDefinition = "VARCHAR(255) COMMENT 'tag of the system configuration'")
    private Integer activeInd;

    @ManyToOne
    @JoinColumn(name = "group_id", referencedColumnName = "id")
    private TelegramGroupEntity telegramGroupEntity;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private TelegramUserEntity telegramUserEntity;

    @OneToMany(mappedBy = "gameRecordEntity", fetch = FetchType.LAZY)
    private List<GameDetailsEntity> gameDetailsEntities = new ArrayList<>();

    public TelegramGroupEntity getTelegramGroupEntity() {
        return telegramGroupEntity;
    }

    public void setTelegramGroupEntity(TelegramGroupEntity telegramGroupEntity) {
        this.telegramGroupEntity = telegramGroupEntity;
    }

    public TelegramUserEntity getTelegramUserEntity() {
        return telegramUserEntity;
    }

    public void setTelegramUserEntity(TelegramUserEntity telegramUserEntity) {
        this.telegramUserEntity = telegramUserEntity;
    }

    public Integer getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(Integer activeInd) {
        this.activeInd = activeInd;
    }

    public List<GameDetailsEntity> getGameDetailsEntities() {
        return gameDetailsEntities;
    }

    public void setGameDetailsEntities(List<GameDetailsEntity> gameDetailsEntities) {
        this.gameDetailsEntities = gameDetailsEntities;
    }
}
