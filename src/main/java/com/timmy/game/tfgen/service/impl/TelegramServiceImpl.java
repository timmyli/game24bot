package com.timmy.game.tfgen.service.impl;

import com.timmy.game.tfgen.dao.entity.*;
import com.timmy.game.tfgen.dao.repo.SystemConfigRepository;
import com.timmy.game.tfgen.dao.repo.TelegramGroupRepository;
import com.timmy.game.tfgen.dao.repo.TelegramUserRepository;
import com.timmy.game.tfgen.service.SystemConfigService;
import com.timmy.game.tfgen.service.TelegramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional(value = "transactionManager")
public class TelegramServiceImpl implements TelegramService {

    @Autowired
    private TelegramGroupRepository groupRepository;

    @Autowired
    private TelegramUserRepository userRepository;

    @Override
    public boolean isUserExist(String chatId) {
        if(userRepository.findOne(QTelegramUserEntity.telegramUserEntity.telegramId.eq(chatId)).orElse(null)!=null)
        return true;
        return false;
    }

    @Override
    public boolean isGroupExist(String groupChatId) {
        if(groupRepository.findOne(QTelegramGroupEntity.telegramGroupEntity.groupChatId.eq(groupChatId)).orElse(null)!=null)
            return true;
        return false;
    }

    @Override
    public TelegramUserEntity createTelegramUser(String telegramId,String username) {
        TelegramUserEntity userEntity = new TelegramUserEntity();
        userEntity.setTelegramId(telegramId);
        userEntity.setTelegramChatId(telegramId);
        userEntity.setUsername(username);
        userEntity.setCreatedBy(0L);
        userEntity.setCreatedDt(new Date());
        return userRepository.save(userEntity);
    }

    @Override
    public TelegramGroupEntity createGroup(String groupChatId) {
        TelegramGroupEntity groupEntity = new TelegramGroupEntity();
        groupEntity.setGroupChatId(groupChatId);
        groupEntity.setCreatedBy(0L);
        groupEntity.setCreatedDt(new Date());
        return groupRepository.save(groupEntity);
    }


}
