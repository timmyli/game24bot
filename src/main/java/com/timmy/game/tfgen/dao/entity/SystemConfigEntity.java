package com.timmy.game.tfgen.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "sys01_base_configuration")
public class SystemConfigEntity extends BaseEntity{

    @Column(name = "tag", columnDefinition = "VARCHAR(255) COMMENT 'tag of the system configuration'")
    private String tag;

    @Column(name = "key", columnDefinition = "VARCHAR(255) COMMENT 'key of the system configuration'")
    private String key;

    @Column(name="value", columnDefinition = "VARCHAR(255) COMMENT 'value of the system configuration'")
    private String value;

    @Column(name="description", columnDefinition = "VARCHAR(255) COMMENT 'description of the system configuration'")
    private String description;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
