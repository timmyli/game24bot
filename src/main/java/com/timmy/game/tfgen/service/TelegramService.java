package com.timmy.game.tfgen.service;


import com.timmy.game.tfgen.dao.entity.TelegramGroupEntity;
import com.timmy.game.tfgen.dao.entity.TelegramUserEntity;

public interface TelegramService {
   boolean isUserExist(String chatId);

   boolean isGroupExist(String groupChatId);

   TelegramUserEntity createTelegramUser(String telegramId,String username);

   TelegramGroupEntity createGroup(String groupChatId);

}
