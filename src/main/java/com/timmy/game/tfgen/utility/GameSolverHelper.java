package com.timmy.game.tfgen.utility;

import com.timmy.game.tfgen.utility.exception.InvalidActiveGameException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

public class GameSolverHelper {

    public static ArrayList<Integer> getRandomSolvableList(int minimum,int maximum,int noOfElement){
        ArrayList<Integer> randomList = new ArrayList();
        if (noOfElement< 3) {
            throw new InvalidActiveGameException("No of elements must be greater than 4");
        }
        int tempNo = noOfElement;

        int noOfTry = 0 ;
        while(tempNo > 0){
            tempNo--;
            int randomNum = minimum + (int)(Math.random() * ((maximum - minimum) + 1));
            randomList.add(randomNum);
        }

        if (isSolvableSolution(randomList)){
            return randomList;
        } else {
           return getRandomSolvableList(minimum,maximum,noOfElement);
        }

    }
    private static boolean isSolvableSolution (List<Integer> listOfElement){
        boolean isSolvable = false;
        for (Expression exp : getExpressions(listOfElement)){
            if ((exp.evaluate() == 24)) {
                isSolvable = true;
                break;
            }
        }
        return isSolvable;
    }
    public static String getSolution (List<Integer> listOfElement){
        StringBuilder sb = new StringBuilder();
        int i = 1;
        for (Expression exp : getExpressions(listOfElement)){
            if (i == 1 && (exp.evaluate() == 24)) {
                sb.append(exp);
                i++;
            }else if (i!=1) break;
        }
        return sb.toString();
    }

    public static String getAllSolution (List<Integer> listOfElement){
        StringBuilder sb = new StringBuilder();
        for (Expression exp : getExpressions(listOfElement)){
            if ((exp.evaluate() == 24)) {
                sb.append(exp + "\n");
            }
        }
        return sb.toString();
    }
//    public static void main(String[] _) {
//        for (Expression exp : getExpressions(Arrays.asList(3, 3	, 8, 8)))
//            if (exp.evaluate() == 24)
//                System.out.println(exp);
//    }

    static Set<Expression> getExpressions(Collection<Integer> numbers) {
        Set<Expression> expressions = new HashSet<Expression>();
        if (numbers.size() == 1) {
            expressions.add(new ConstantExpression(numbers.iterator().next()));
            return expressions;
        }

        for (Collection<Collection<Integer>> grouping : getGroupings(numbers))
            if (grouping.size() > 1)
                expressions.addAll(getExpressionsForGrouping(grouping));
        return expressions;
    }

    static Set<Expression> getExpressionsForGrouping(Collection<Collection<Integer>> groupedNumbers) {
        Collection<Set<Expression>> groupExpressionOptions = new ArrayList<Set<Expression>>();
        for (Collection<Integer> group : groupedNumbers)
            groupExpressionOptions.add(getExpressions(group));

        Set<Expression> result = new HashSet<Expression>();
        for (Collection<Expression> expressions : getCombinations(groupExpressionOptions)) {
            boolean containsAdditive = false, containsMultiplicative = false;
            for (Expression exp : expressions) {
                containsAdditive |= exp instanceof AdditiveExpression;
                containsMultiplicative |= exp instanceof MultiplicativeExpression;
            }

            Expression firstExpression = expressions.iterator().next();
            Collection<Expression> restExpressions = new ArrayList<Expression>(expressions);
            restExpressions.remove(firstExpression);

            for (int i = 0; i < 1 << restExpressions.size(); ++i) {
                Iterator<Expression> restExpressionsIter = restExpressions.iterator();
                Collection<Expression> a = new ArrayList<Expression>(), b = new ArrayList<Expression>();
                for (int j = 0; j < restExpressions.size(); ++j)
                    Arrays.asList(a, b).get(i >> j & 1).add(restExpressionsIter.next());
                if (!containsAdditive)
                    result.add(new AdditiveExpression(firstExpression, a, b));
                if (!containsMultiplicative)
                    try {
                        result.add(new MultiplicativeExpression(firstExpression, a, b));
                    } catch (ArithmeticException e) {}
            }
        }
        return result;
    }

    // Sample input/output:
    // [ {a,b} ]               -> { [a], [b] }
    // [ {a,b}, {a} ]          -> { [a,b], [a,a] }
    // [ {a,b,c}, {d}, {e,f} ] -> { [a,d,e], [a,d,f], [b,d,e], [b,d,f], [c,d, e], [c,d,f] }
    static <T> Set<Collection<T>> getCombinations(Collection<Set<T>> collectionOfOptions) {
        if (collectionOfOptions.isEmpty())
            return new HashSet<Collection<T>>() {{ add(new ArrayList<T>()); }};

        Set<T> firstOptions = collectionOfOptions.iterator().next();
        Collection<Set<T>> restCollectionOfOptions = new ArrayList<Set<T>>(collectionOfOptions);
        restCollectionOfOptions.remove(firstOptions);

        Set<Collection<T>> result = new HashSet<Collection<T>>();
        for (T first : firstOptions)
            for (Collection<T> restCombination : getCombinations(restCollectionOfOptions))
                result.add(Util.concat(restCombination, first));
        return result;
    }

    static <T> Set<Collection<Collection<T>>> getGroupings(final Collection<T> values) {
        Set<Collection<Collection<T>>> result = new HashSet<Collection<Collection<T>>>();
        if (values.isEmpty()) {
            result.add(new ArrayList<Collection<T>>());
            return result;
        }

        for (Collection<T> firstGroup : getSubcollections(values)) {
            if (firstGroup.size() == 0)
                continue;

            Collection<T> rest = new ArrayList<T>(values);
            for (T value : firstGroup)
                rest.remove(value);

            for (Collection<Collection<T>> restGrouping : getGroupings(rest)) {
                result.add(Util.concat(restGrouping, firstGroup));
            }
        }
        return result;
    }

    static <T> Set<Collection<T>> getSubcollections(final Collection<T> values) {
        if (values.isEmpty())
            return new HashSet<Collection<T>>() {{ add(values); }};

        T first = values.iterator().next();
        Collection<T> rest = new ArrayList<T>(values);
        rest.remove(first);

        Set<Collection<T>> result = new HashSet<Collection<T>>();
        for (Collection<T> subcollection : getSubcollections(rest)) {
            result.add(subcollection);
            result.add(Util.concat(subcollection, first));
        }
        return result;
    }
}

abstract class Expression {
    abstract double evaluate();

    @Override
    public abstract boolean equals(Object o);

    @Override
    public int hashCode() {
        return new Double(evaluate()).hashCode();
    }

    @Override
    public abstract String toString();
}

abstract class AggregateExpression extends Expression {}

class AdditiveExpression extends AggregateExpression {
    final Expression firstOperand;
    final Collection<Expression> addOperands;
    final Collection<Expression> subOperands;

    AdditiveExpression(Expression firstOperand, Collection<Expression> addOperands,
                       Collection<Expression> subOperands) {
        this.firstOperand = firstOperand;
        this.addOperands = addOperands;
        this.subOperands = subOperands;
    }

    @Override
    double evaluate() {
        double result = firstOperand.evaluate();
        for (Expression exp : addOperands)
            result += exp.evaluate();
        for (Expression exp : subOperands)
            result -= exp.evaluate();
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof AdditiveExpression) {
            AdditiveExpression that = (AdditiveExpression) o;
            return Util.equalsIgnoreOrder(Util.concat(this.addOperands, this.firstOperand),
                    Util.concat(that.addOperands, that.firstOperand))
                    && Util.equalsIgnoreOrder(this.subOperands, that.subOperands);
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(firstOperand.toString());
        for (Expression exp : addOperands)
            sb.append('+').append(exp);
        for (Expression exp : subOperands)
            sb.append('-').append(exp);
        return sb.toString();
    }
}

class MultiplicativeExpression extends AggregateExpression {
    final Expression firstOperand;
    final Collection<Expression> mulOperands;
    final Collection<Expression> divOperands;

    MultiplicativeExpression(Expression firstOperand, Collection<Expression> mulOperands,
                             Collection<Expression> divOperands) {
        this.firstOperand = firstOperand;
        this.mulOperands = mulOperands;
        this.divOperands = divOperands;
        for (Expression exp : divOperands)
            if (exp.evaluate() == 0.0)
                throw new ArithmeticException();
    }

    @Override
    double evaluate() {
        double result = firstOperand.evaluate();
        for (Expression exp : mulOperands)
            result *= exp.evaluate();
        for (Expression exp : divOperands)
            result /= exp.evaluate();
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof MultiplicativeExpression) {
            MultiplicativeExpression that = (MultiplicativeExpression) o;
            return Util.equalsIgnoreOrder(Util.concat(this.mulOperands, this.firstOperand),
                    Util.concat(that.mulOperands, that.firstOperand))
                    && Util.equalsIgnoreOrder(this.divOperands, that.divOperands);
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(maybeAddParens(firstOperand));
        for (Expression exp : mulOperands)
            sb.append('*').append(maybeAddParens(exp));
        for (Expression exp : divOperands)
            sb.append('/').append(maybeAddParens(exp));
        return sb.toString();
    }

    static String maybeAddParens(Expression exp) {
        return String.format(exp instanceof AdditiveExpression ? "(%s)" : "%s", exp);
    }
}

class ConstantExpression extends Expression {
    final int value;

    ConstantExpression(int value) {
        this.value = value;
    }

    @Override
    double evaluate() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof ConstantExpression && value == ((ConstantExpression) o).value;
    }

    @Override
    public String toString() {
        return Integer.toString(value);
    }
}

class Util {
    static <T> boolean equalsIgnoreOrder(Collection<T> a, Collection<T> b) {
        Map<T, Integer> aCounts = new HashMap<T, Integer>(), bCounts = new HashMap<T, Integer>();
        for (T value : a) aCounts.put(value, (aCounts.containsKey(value) ? aCounts.get(value) : 0) + 1);
        for (T value : b) bCounts.put(value, (bCounts.containsKey(value) ? bCounts.get(value) : 0) + 1);
        return aCounts.equals(bCounts);
    }

    static <T> Collection<T> concat(Collection<T> xs, final T x) {
        List<T> result = new ArrayList<T>(xs);
        result.add(x);
        return result;
    }
}