package com.timmy.game.tfgen.service;

import org.telegram.telegrambots.meta.api.objects.Update;

public interface TelegramBotService {
    String processReturnMessage(Update update);
    Object getExecutableMessage(Update update);
}
