package com.timmy.game.tfgen.utility;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Helper {
    private static final Logger LOG = LoggerFactory.getLogger(Helper.class);
    public static int maximum = 10;
    public static int minimum = 1;
    public static int numberOfElements = 4;
//    public static List<Integer> generateRandomNumber (){
//        List<Integer> randomList = new ArrayList<>();
//        Random rn = new Random();
//        int i =0;
//        while(i < 4){
//            i++;
//            randomList.add( rn.nextInt(10 - 1 + 1) + 1);
//        }
//        return randomList;
//    }

    public static String randomListToString(){
        List<Integer> randomList = GameSolverHelper.getRandomSolvableList(minimum,maximum,numberOfElements);
        StringBuilder sb = new StringBuilder();
        for (Integer i : randomList){
            sb.append(i + " , ");
        }
        String randomString = sb.substring(0, sb.length() - 3);
        return randomString;
    }
}
