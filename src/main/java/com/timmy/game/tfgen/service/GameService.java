package com.timmy.game.tfgen.service;


import com.timmy.game.tfgen.dao.entity.GameRecordEntity;
import com.timmy.game.tfgen.dto.ChatType;
import com.timmy.game.tfgen.dto.GameDTO;

import java.util.List;

public interface GameService {
    GameRecordEntity startNewGame(String chatId, ChatType chatType);
    GameDTO startNewRound(String chatId, ChatType chatType);
    GameDTO getSolution(String chatId, ChatType chatType);
    GameDTO getAllSolution(String chatId, ChatType chatType);
}
