package com.timmy.game.tfgen.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {
    @GetMapping("/test")
    public String test(){
        String x = "test";
        System.out.println(x);
        return "testresult";
    }

    @GetMapping("/")
    public String test2(){
        String x = "test";
        System.out.println(x);
        return "testresult";
    }
}
